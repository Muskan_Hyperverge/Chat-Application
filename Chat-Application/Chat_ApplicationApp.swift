//
//  Chat_ApplicationApp.swift
//  Chat-Application
//
//  Created by Muskan Ludhiyani on 21/02/22.
//

import SwiftUI

@main
struct Chat_ApplicationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
