//
//  ContentView.swift
//  Chat-Application
//
//  Created by Muskan Ludhiyani on 21/02/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
